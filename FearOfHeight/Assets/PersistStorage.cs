﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;

public class PersistStorage : MonoBehaviour
{
    private int loadCount;
    public int LoadCount
    {
        get
        {
            return loadCount;
        }
        set
        {
            this.loadCount = value;
            MySpatialProcessingTest.Instance.appendSubText("Load:" + loadCount.ToString() + "Times");
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        loadCount = 0;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
