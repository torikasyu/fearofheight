﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.AI;

public class MoveToTapPoint : MonoBehaviour
{

    public GameObject NavMeshAgentObject;
    NavMeshAgent Agent;
    LineRenderer lineRenderer;
    //private float floorYPos;

    public delegate void MyCallback();
    //private MyCallback callbacks;

    private bool completed = false;
    //private bool callbacked = false;

    NavMeshPath path;
    Vector3 startPos;
    Vector3 goalPos;

    void Start()
    {
        InputManager.Instance.PushFallbackInputHandler(gameObject);

        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.positionCount = 0;
    }

    void Update()
    {
        if (Agent != null) { print("AgentStatus:" + Agent.pathStatus); }

        if (!completed && Agent != null &&
            (Agent.pathStatus == NavMeshPathStatus.PathComplete || Agent.pathStatus == NavMeshPathStatus.PathInvalid))
        {
            completed = true;
            MakeRoute();
        }

        /*
        if(completed && callbacks != null && !callbacked)
        {
            callbacked = true;
            callbacks();
        }
        */
    }

    /*
    public void addCallback(MyCallback callback)
    {
        // コールバックを登録します
        callbacks += callback;
    }
    */

    public void CreateRoute(Vector3 _goalPos)
    {
        this.goalPos = _goalPos;
        this.startPos = new Vector3(Camera.main.transform.position.x, _goalPos.y, Camera.main.transform.position.z);

        print("StartPos:" + startPos);
        print("GoalPos:" + goalPos);

        var obj = Instantiate(NavMeshAgentObject, startPos, Quaternion.identity);
        Agent = obj.GetComponent<NavMeshAgent>();

        Agent.gameObject.SetActive(true);
        // 上に持ち上げる→不要だった
        //Vector3 agentStartPos = new Vector3(startPos.x, startPos.y + NavMeshAgentObject.transform.localScale.y/2, startPos.z);

        Agent.transform.position = startPos;
        Agent.GetComponent<NavMeshAgent>().Warp(startPos);
        Agent.destination = goalPos;

        path = new NavMeshPath();
        NavMesh.CalculatePath(Agent.transform.position, Agent.destination, NavMesh.AllAreas, path);
    }

    void MakeRoute()
    {
        // パスの計算
        var positions = path.corners;

        // ルートの描画
        lineRenderer.widthMultiplier = 1f;
        lineRenderer.positionCount = positions.Length;

        GameObject floorPalent = GameObject.Find("BrickPalent");
        foreach (Transform go in floorPalent.transform)
        {
            go.gameObject.GetComponent<Rock>().isFallable = true;
        }

        for (int i = 0; i < positions.Length; i++)
        {
            //lineRenderer.SetPosition(i, positions[i]);

            if (i != positions.Length - 1)
            {
                makeNotFallable(positions[i], positions[i + 1]);
            }
        }
        if (positions.Length > 0)
        {
            // ルートが途中までできたが、ゴールに達していないときに最後のポイントからゴールまで作成
            if (Vector3.SqrMagnitude(positions[positions.Length - 1] - goalPos) > 1f)
            {
                makeNotFallable(positions[positions.Length - 1], goalPos);
            }
        }
        else
        {
            // ルートが全く作れなかったのでゴールまでのルートを作成
            makeNotFallable(startPos, goalPos);
        }

        //MySpatialProcessingTest.Instance.appendSubText("Route created(" + positions.Length + ")");
        print("Route created(" + positions.Length + ")");
    }

    void makeNotFallable(Vector3 from, Vector3 to)
    {
        int mask = LayerMask.GetMask("Brick");

        Vector3 direction = to - from;
        float distance = Vector3.Distance(to, from);

        RaycastHit[] hits;
        hits = Physics.SphereCastAll(from, 0.3f, direction, distance, mask);

        foreach (RaycastHit h in hits)
        {
            h.transform.gameObject.GetComponent<Rock>().isFallable = false;
        }
    }
}