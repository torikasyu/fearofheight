﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour {

    AudioSource[] audioSources;
    Animator motion;

    public float interval = 3.0f;
    float counter = 0.0f;

    public bool playSourceComeOn = false;

    bool isGoal = false;

    // Use this for initialization
    void Start () {
        audioSources = GetComponents<AudioSource>();
        motion = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

        if (playSourceComeOn) { Clock(Time.deltaTime);}

        Vector3 goalHeadPos = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);

		if(!isGoal && Vector3.SqrMagnitude(goalHeadPos - Camera.main.transform.position) < 1.5f) {

            isGoal = true;
            playSourceComeOn = false;

            motion.SetBool("Win", true);
            audioSources[1].Play();

            MySpatialProcessingTest.Instance.GoalArcheved();
        }
	}

    void Clock(float t)
    {
        counter += t;
        if (counter > interval)
        {
            audioSources[0].Play();
            counter = 0.0f;
        }
    }
}
