﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;
using UnityEngine.SceneManagement;

//namespace HoloToolkit.Unity.SpatialMapping.Tests
//{
/// <summary>
/// The SpatialProcessingTest class allows applications to scan the environment for a specified amount of time 
/// and then process the Spatial Mapping Mesh (find planes, remove vertices) after that time has expired.
/// </summary>
public class MySpatialProcessingTest : Singleton<MySpatialProcessingTest>, IInputClickHandler
{
    [Tooltip("How much time (in seconds) that the SurfaceObserver will run after being started; used when 'Limit Scanning By Time' is checked.")]
    public float scanTime = 60.0f;

    [Tooltip("Material to use when rendering Spatial Mapping meshes while the observer is running.")]
    public Material defaultMaterial;

    [Tooltip("Optional Material to use when rendering Spatial Mapping meshes after the observer has been stopped.")]
    public Material secondaryMaterial;

    [Tooltip("Minimum number of floor planes required in order to exit scanning/processing mode.")]
    public uint minimumFloors = 1;

    // for placement
    public GameObject toPlaceObj;
    private GameObject toPlaceObjInstance;
    // Consts
    public float kMinAreaForStats = 5.0f;
    public float kMinAreaForComplete = 10.0f;
    public float kMinHorizAreaForComplete = 10.0f;
    public float kMinWallAreaForComplete = 10.0f;

    public SpatialMappingObserver MappingObserver;

    private bool _scanfinishTriggered = false;
    private bool _placementTriggered = false;
    private bool _makePlanesTriggered = false;

    // ゴールの大きさ（PlacementDllが使用する）
    private Vector3 boxFullDims = new Vector3(0.2f, 0.2f, 1.5f);
    private Vector3 goalPosition = Vector3.zero;

    public Text mainText;
    public Text subText;

    private float floorYPos;
    private GameObject landscape;

    //ゲームクリアしているかどうか
    private bool isCleared = false;
    private bool gameStared = false;

    // Goalから呼ばれる
    public void GoalArcheved()
    {
        isCleared = true;
        this.mainText.text = "You have made GOAL !";
        this.subText.text = "AirTap to restart.";
    }

    public void appendSubText(string t)
    {
        if (this.subText.text != "")
        {
            this.subText.text += "\n" + t;
        }
        else
        {
            this.subText.text = t;
        }
    }

    public bool DoesScanMeetMinBarForCompletion
    {
        get
        {
            // Only allow this when we are actually scanning
            if ((SpatialUnderstanding.Instance.ScanState != SpatialUnderstanding.ScanStates.Scanning) ||
                (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding))
            {
                return false;
            }

            // Query the current playspace stats
            IntPtr statsPtr = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceStatsPtr();
            if (SpatialUnderstandingDll.Imports.QueryPlayspaceStats(statsPtr) == 0)
            {
                return false;
            }
            SpatialUnderstandingDll.Imports.PlayspaceStats stats = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceStats();

            string warnText = "";

            float rateHoriz = stats.HorizSurfaceArea / kMinHorizAreaForComplete;
            float rateWall = stats.WallSurfaceArea / kMinWallAreaForComplete;
            if (rateHoriz <= 1f && rateWall <= 1f) { warnText = "Scan more horizontal and wall surface."; }
            else if (rateHoriz <= 1f) { warnText = "Scan more horizontal surface."; }
            else if (rateWall <= 1f) { warnText = "Scan more wall surface."; }

            subText.text = String.Format("{0}\nFloor {1:#0}%\nWall {2:#0}%",
                warnText,
                rateHoriz * 100,
                rateWall * 100
            );

            if ((stats.HorizSurfaceArea > kMinHorizAreaForComplete) &&
                (stats.WallSurfaceArea > kMinWallAreaForComplete))
            {
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// Indicates if processing of the surface meshes is complete.
    /// </summary>
    //private bool meshesProcessed = false;

    void CreateRoute()
    {
        // LocalNavMeshBuilderの処理を待つ
        this.mainText.text = "Creating goal.";
        StartCoroutine("waitFor");

        MoveToTapPoint moveTo = gameObject.GetComponent<MoveToTapPoint>();
        //moveTo.addCallback(OnCreateRouteCompleted);

        toPlaceObjInstance.GetComponent<Animator>().SetBool("HandUp", true);
        toPlaceObjInstance.GetComponent<GoalManager>().playSourceComeOn = true;
        this.mainText.text = "Let's find & go to Unity-Chan!";

        moveTo.CreateRoute(goalPosition);

        this.gameStared = true;
    }

    // moveTo.CreateRoute後に呼ばれるコールバック関数
    /*
    public void OnCreateRouteCompleted()
    {
        toPlaceObjInstance.GetComponent<Animator>().SetBool("HandUp", true);
        toPlaceObjInstance.GetComponent<GoalManager>().playSourceComeOn = true;

        this.mainText.text = "Let's find & go to Unity-Chan!";
    }
    */

    IEnumerator waitFor()
    {
        yield return new WaitForSeconds(5f);
    }

    /// <summary>
    /// GameObject initialization.
    /// </summary>
    private void Start()
    {
        // Update surfaceObserver and storedMeshes to use the same material during scanning.
        SpatialMappingManager.Instance.SetSurfaceMaterial(defaultMaterial);

        // Register for the MakePlanesComplete event.
        SurfaceMeshesToPlanes.Instance.MakePlanesComplete += SurfaceMeshesToPlanes_MakePlanesComplete;

        //Vector3 sceneOrigin = Camera.main.transform.position;
        //Parent_Scene.transform.position = sceneOrigin;
        //MappingObserver.SetObserverOrigin(sceneOrigin);

        landscape = GameObject.Find("Landscape");
        landscape.SetActive(false);

        this.mainText.text = "Walk around and scan in your playspace.";
    }

    private float ScannedTime
    {
        get
        {
            return Time.unscaledTime - SpatialMappingManager.Instance.StartTime;
        }
    }

    /// <summary>
    /// Called once per frame.
    /// </summary>
    private void Update()
    {
        if (!_scanfinishTriggered &&
            (DoesScanMeetMinBarForCompletion &&
            (SpatialUnderstanding.Instance.ScanState == SpatialUnderstanding.ScanStates.Scanning) && !SpatialUnderstanding.Instance.ScanStatsReportStillWorking)
        )
        {
            _scanfinishTriggered = true;
            print("FinishScan Requested");
            SpatialUnderstanding.Instance.RequestFinishScan();
        }

        if (!_scanfinishTriggered && ScannedTime > scanTime)
        {
            _scanfinishTriggered = true;
            print("FinishScan Requested by Time");
            SpatialUnderstanding.Instance.RequestFinishScan();
        }

        if (!_placementTriggered && SpatialUnderstanding.Instance.ScanState == SpatialUnderstanding.ScanStates.Done)
        {
            print("PlacementTriggered");

            _placementTriggered = true;

            if (SpatialMappingManager.Instance.IsObserverRunning())
            {
                // Stop the observer.
                SpatialMappingManager.Instance.StopObserver();
            }

            //SpatialUnderstandingを用いたゴールの作成
            CreateGoal();
        }

        if (_makePlanesTriggered)
        {
            _makePlanesTriggered = false;

            //Planeの作成、Brickの生成、ルートの作成（MakePlanesComplete）
            SurfaceMeshesToPlanes surfaceToPlanes = SurfaceMeshesToPlanes.Instance;
            surfaceToPlanes.MakePlanes();
        }

        if (gameStared)
        {
            // カメラを向いてもらう
            var dif = Camera.main.transform.position - toPlaceObjInstance.transform.position;
            var rot = Quaternion.LookRotation(new Vector3(dif.x, 0, dif.z));

            this.toPlaceObjInstance.transform.rotation = rot;
        }

        /*
        // Check to see if the spatial mapping data has been processed yet.
        if (!meshesProcessed)
        {
            // Check to see if enough scanning time has passed
            // since starting the observer.
            if ((Time.unscaledTime - SpatialMappingManager.Instance.StartTime) < scanTime)
            {
                // If we have a limited scanning time, then we should wait until
                // enough time has passed before processing the mesh.
            }
            else
            {
                // The user should be done scanning their environment,
                // so start processing the spatial mapping data...

                if (SpatialMappingManager.Instance.IsObserverRunning())
                {
                    // Stop the observer.
                    SpatialMappingManager.Instance.StopObserver();
                }

                // Call CreatePlanes() to generate planes.
                CreatePlanes();

                // Set meshesProcessed to true.
                meshesProcessed = true;
            }
        }
        */
    }

    /// <summary>
    /// Handler for the SurfaceMeshesToPlanes MakePlanesComplete event.
    /// </summary>
    /// <param name="source">Source of the event.</param>
    /// <param name="args">Args for the event.</param>
    private void SurfaceMeshesToPlanes_MakePlanesComplete(object source, System.EventArgs args)
    {
        // Collection of floor planes that we can use to set horizontal items on.
        List<GameObject> floors = new List<GameObject>();
        floors = SurfaceMeshesToPlanes.Instance.GetActivePlanes(PlaneTypes.Floor);

        // Check to see if we have enough floors (minimumFloors) to start processing.
        if (floors.Count >= minimumFloors)
        {
            // ブロックの作成
            MakeBricks(floors);

            // 床Planeの周りのポリゴン削除
            RemoveVertices(floors);

            // After scanning is over, switch to the secondary (occlusion) material.
            SpatialMappingManager.Instance.SetSurfaceMaterial(secondaryMaterial);
        }
        else
        {
            // Re-enter scanning mode so the user can find more surfaces before processing.
            //SpatialMappingManager.Instance.StartObserver();

            // Re-process spatial data after scanning completes.
            //meshesProcessed = false;
            mainText.text = "Sorry,Needs Re-scan.";
            isCleared = true;
        }

        // 最終処理
        //航空写真の表示
        landscape.SetActive(true);

        // Understandingを削除
        SpatialUnderstanding.Instance.gameObject.SetActive(false);

        //NavMesh生成開始
        gameObject.GetComponent<LocalNavMeshBuilder>().enabled = true;

        // ゴールまでのルートの作成
        CreateRoute();
    }

    public GameObject brick;
    public float floorExpandSize = 0;   // 生成されたPlaneよりもブロックを並べる範囲を大きくする

    void MakeBricks(List<GameObject> floors)
    {
        // ブロックのサイズ（立方体想定）
        float brickSize = 0.3f;
        GameObject largest = null;

        // 生成されたPlaneの中で最大のものを採用
        foreach (GameObject f in floors)
        {
            if (f.transform.localScale.x > 3f && f.transform.localScale.y > 3f)
            {
                if (largest == null || (largest.transform.localScale.sqrMagnitude < f.transform.localScale.sqrMagnitude))
                {
                    largest = f;
                }
            }
        }

        if (largest == null)
        {
            print("largest is null");
            mainText.text = "Sorry,Needs Re-scan. AirTap please.";
            isCleared = true;
        }
        else
        {
            // Planeよりも指定数だけブロックを並べる範囲を拡張する
            largest.transform.localScale = new Vector3(largest.transform.localScale.x + floorExpandSize, largest.transform.localScale.y + floorExpandSize, largest.transform.localScale.z);

            // ブロックの親オブジェクト
            var parent = new GameObject("BrickPalent");

            // ブロックの個数計算
            int repx = (int)Mathf.Floor(largest.transform.localScale.x / brickSize);
            int repy = (int)Mathf.Floor(largest.transform.localScale.y / brickSize);

            // 並べ始める最初の位置
            float xpos = -largest.transform.localScale.x / 2;
            float ypos = -largest.transform.localScale.y / 2;
            float zpos = brickSize / 2;

            // 最初のY座標を退避
            float yposOrigin = ypos;

            for (int i = 0; i < repx; i++)
            {
                for (int j = 0; j < repy; j++)
                {
                    var go2 = Instantiate(this.brick, new Vector3(xpos, ypos, zpos), Quaternion.identity);
                    go2.transform.SetParent(parent.transform);

                    if (true) //(UnityEngine.Random.Range(0, 10) > 1)
                    {
                        go2.AddComponent<NavMeshSourceTag>();
                    }

                    ypos += brickSize;
                }
                ypos = yposOrigin;

                xpos += brickSize;
            }

            //自分とゴールの位置の中間に配置したい
            Vector3 c = Camera.main.transform.position;
            Vector3 g = this.goalPosition;
            Vector3 harfPos = new Vector3((c.x + g.x) / 2, this.floorYPos, (c.z + g.z) / 2);

            parent.transform.position = harfPos;
            parent.transform.rotation = largest.transform.rotation;
        }
    }

    /// <summary>
    /// Removes triangles from the spatial mapping surfaces.
    /// </summary>
    /// <param name="boundingObjects"></param>
    private void RemoveVertices(IEnumerable<GameObject> boundingObjects)
    {
        RemoveSurfaceVertices removeVerts = RemoveSurfaceVertices.Instance;
        if (removeVerts != null && removeVerts.enabled)
        {
            removeVerts.UpperY = this.floorYPos;
            removeVerts.RemoveSurfaceVerticesWithinBounds(boundingObjects);
        }
    }

    /// <summary>
    /// Called when the GameObject is unloaded.
    /// </summary>
    protected override void OnDestroy()
    {
        if (SurfaceMeshesToPlanes.Instance != null)
        {
            SurfaceMeshesToPlanes.Instance.MakePlanesComplete -= SurfaceMeshesToPlanes_MakePlanesComplete;
        }

        base.OnDestroy();
    }

    // Understandingを用いて床の座標を求める
    float getFloorY()
    {
        // First clear all our geo
        // ClearGeometry();

        // Only if we're enabled
        if (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding)
        {
            return 0;
        }

        // Alignment information
        SpatialUnderstandingDll.Imports.QueryPlayspaceAlignment(SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignmentPtr());
        SpatialUnderstandingDll.Imports.PlayspaceAlignment alignment = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticPlayspaceAlignment();

        return alignment.FloorYValue;
    }


    void CreateGoal()  //PlacementDllを使ったゴールの作成
    {
        this.floorYPos = getFloorY();   //PlacementDllを使ったfloorYの取得

        // DLLの初期化
        SpatialUnderstandingDllObjectPlacement.Solver_Init();

        var halfBoxDims = boxFullDims * .5f;
        // 他のオブジェクトから離す距離
        var disctanceFromOtherObjects = halfBoxDims.x > halfBoxDims.z ? halfBoxDims.x * 3f : halfBoxDims.z * 3f;
        // 作成したいオブジェクトの数
        var desiredLocationCount = 1;

        for (int i = 0; i < desiredLocationCount; ++i)
        {
            // ルールの作成（複数追加可能）
            var placementRules = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>();
            placementRules.Add(SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromPosition(Camera.main.transform.position, 2f));

            // 制約の作成（複数追加可能）
            var placementConstraints = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint>();
            placementConstraints.Add(SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint.Create_AwayFromPoint(Camera.main.transform.position));

            // 定義の作成（１つだけ）
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition placementDefinition = SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(halfBoxDims);

            int ret = SpatialUnderstandingDllObjectPlacement.Solver_PlaceObject(
                "my placement",
                SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementDefinition),
                placementRules.Count,
                SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementRules.ToArray()),
                placementConstraints.Count,
                SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(placementConstraints.ToArray()),
                SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticObjectPlacementResultPtr()
            );

            print("Placement request return:" + ret.ToString());
            //subText.text = "Placement request return(" + ret.ToString() + ")";

            if (ret > 0)
            {
                SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult placementResult = SpatialUnderstanding.Instance.UnderstandingDLL.GetStaticObjectPlacementResult();

                //var rotation = Quaternion.LookRotation(placementResult.Forward, Vector3.up);
                var rotation = Quaternion.LookRotation(Vector3.up);

                // ゴールの作成
                var obj = Instantiate(toPlaceObj, new Vector3(placementResult.Position.x, this.floorYPos, placementResult.Position.z), Quaternion.identity);
                print("Instance" + obj.transform.localRotation);

                // モーション設定
                var motion = obj.GetComponent<Animator>();
                motion.SetBool("HandUp", true);

                // カメラを向いてもらう
                var dif = Camera.main.transform.position - gameObject.transform.position;
                var rot = Quaternion.LookRotation(new Vector3(dif.x, 0, dif.z));
                obj.transform.rotation = rot;

                //obj.transform.LookAt(Camera.main.transform.position,Vector3.up);
                //obj.transform.rotation = Quaternion.Euler(new Vector3(0, obj.transform.rotation.y, 0));
                //print("LookAt" + obj.transform.localRotation);

                // インスタンスをグローバル変数にセット
                toPlaceObjInstance = obj;

                // ゴールの位置をグローバル変数にセット
                this.goalPosition = new Vector3(obj.transform.position.x, floorYPos, obj.transform.position.z);

                _makePlanesTriggered = true;
            }
            else
            {
                mainText.text = "Couldn't create scene. Restart after some seconds.";
                StartCoroutine("waitFor");
                SceneManager.LoadScene("main");

                //SpatialUnderstanding.Instance.RequestBeginScanning();
                //_placementTriggered = false;
            }
        }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //throw new NotImplementedException();
        if (isCleared)
        {
            SceneManager.LoadScene("main");
        }
    }
}
//}