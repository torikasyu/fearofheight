﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    // カメラの視界に入ったかどうか
    private bool isInsight = false;

    // 落下処理判定済みかどうか
    private bool processed = false;

    // 落下判定対象かどうか
    public bool isFallable = false;

    private AudioSource aSource;

    void OnWillRenderObject()
    {

#if UNITY_EDITOR
        if (Camera.current.name != "SceneCamera" && Camera.current.name != "Preview Camera")
#endif
        {
            // 処理
            isInsight = true;
        }
    }

    //int[] brick;

    // Use this for initialization
    void Start()
    {
        aSource = GetComponent<AudioSource>();
        //brick = new int[transform.childCount];

    }

    // Update is called once per frame
    void Update()
    {

        /*
        int i = 0;
        foreach (Transform t in gameObject.transform)
        {
            if (Vector3.SqrMagnitude(Camera.main.transform.position - t.position) < 10f)
            {
                if (brick[i] == 0 && !t.gameObject.GetComponent<Rigidbody>())
                {
                    if (Random.Range(0, 5) == 1)
                    {
                        t.gameObject.AddComponent<Rigidbody>();
                    }

                    brick[i] = 1;
                }
            }

            i++;
        }

        if(Input.GetKey(KeyCode.Space))
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z + Time.deltaTime);
        }
        */

        if (isInsight && isFallable)
        {

            if (!processed && Vector3.SqrMagnitude(Camera.main.transform.position - this.transform.position) < 4f)
            {
                //print("near and in sight");
                if (!gameObject.GetComponent<Rigidbody>())
                {
                    if(true) //
                    {
                        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
                        //rb.AddForce(new Vector3(0, 100, 0));
                        if (Random.Range(0, 4) >= 3)
                        {
                            aSource.Play();
                        }
                    }
                }
                processed = true;
            }
        }

        if(gameObject.transform.position.z < -30f)
        {
            Destroy(gameObject);
        }

    }
}
