﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanRotate : MonoBehaviour {

    public GameObject Target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKey(KeyCode.Space))
        {
            // 相対回転）
            gameObject.transform.Rotate(new Vector3(0, Time.deltaTime * 100f, 0), Space.World);
        }

        // 絶対回転
        var cam = Camera.main.transform.position;
        var dif = new Vector3(cam.x - 0.5f,cam.y,cam.z) - gameObject.transform.position;
        var rot = Quaternion.LookRotation(new Vector3(dif.x,0,dif.z));

        gameObject.transform.rotation = rot;


    }
}
